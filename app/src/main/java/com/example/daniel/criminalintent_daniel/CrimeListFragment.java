package com.example.daniel.criminalintent_daniel;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CrimeListFragment extends Fragment {

    private static final String SAVED_SUBTITLE_VISIBLE = "subtitle";
    private RecyclerView mCrimeRecyclerView;
    private CrimeAdapter mAdapter;
    private boolean mIsSubtitleVisible;
    private Callbacks mCallbacks;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // the fragment manager must know that this fragment has an option menu
        setHasOptionsMenu(true);
    }

    /**
     * Method is called when a fragment is attached to an activity.
     *
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallbacks = (Callbacks) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        // set callback to null because I cannot count that the activity still exists
        // after the detach
        mCallbacks = null;
    }

    /**
     * Create a menu for this fragment.
     *
     * @param menu
     * @param inflater
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_crime_list, menu);

        MenuItem subtitleItem = menu.findItem(R.id.show_subtitle);
        if (mIsSubtitleVisible) {
            subtitleItem.setTitle(R.string.hide_subtitle);
        } else {
            subtitleItem.setTitle(R.string.show_subtitle);
        }


    }

    /**
     * will be called if any of the options in the
     * toolbar is selected by the user.
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_crime:
                Crime crime = new Crime();
                CrimeLab crimeLab = CrimeLab.get(getActivity());
                crimeLab.addCrime(crime);
                updateUI();
                mCallbacks.onCrimeSelected(crime);
                return true;
            case R.id.show_subtitle:
                // toggle flag for subtitle
                mIsSubtitleVisible = !mIsSubtitleVisible;
                // trigger a recreation of the subtitle item text:
                getActivity().invalidateOptionsMenu();
                // update number in subtitle
                updateSubtitle();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Generate string for subtitle
     */
    private void updateSubtitle() {
        CrimeLab crimeLab = CrimeLab.get(getActivity());
        int CrimeCount = crimeLab.getCrimes().size();
        String subtitle = getString(R.string.subtitle_format, CrimeCount);

        if (!mIsSubtitleVisible) {
            // do not show subtitle if it should not
            subtitle = null;
        }
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.getSupportActionBar().setSubtitle(subtitle);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_crime_list, container, false);

        // create a RecyclerView:
        mCrimeRecyclerView = (RecyclerView) v.findViewById(R.id.crime_recycler_view);

        // as soon as you create your RecyclerView, you give it another object called a LayoutManger
        // it is required, if not RV will crash
        mCrimeRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        // get Subtitel visiblity after rotation:
        if (savedInstanceState != null) {
            mIsSubtitleVisible = savedInstanceState.getBoolean(SAVED_SUBTITLE_VISIBLE);
        }

        // connect RecyclerView with it's adapter
        updateUI();

        return v;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        // save visibility status for rotation
        outState.putBoolean(SAVED_SUBTITLE_VISIBLE, mIsSubtitleVisible);
    }

    /**
     * page 212.
     * Will be called after the user returns from CrimeActivity to this activity/fragment.
     */
    @Override
    public void onResume() {
        super.onResume();
        // update possible changes
        updateUI();
    }

    /**
     * Method that connects RecyclerView with it's adapter (CrimeAdapter)
     */
    public void updateUI() {
        CrimeLab crimeLab = CrimeLab.get(getActivity());
        List<Crime> crimes = crimeLab.getCrimes();
        if (mAdapter == null) {
            // e.g. after onCreateView -> set up adapter and connect it with recycler view.
            mAdapter = new CrimeAdapter(crimes);
            mCrimeRecyclerView.setAdapter(mAdapter);
        } else {
            // update the "snapshot" of the crimes
            mAdapter.setCrimes(crimes);
            // e.g. after onResume -> update existing crimes.
            mAdapter.notifyDataSetChanged();
        }

        // also update the subtitle
        updateSubtitle();
    }

    /**
     * This interface is required by hosting activities.
     */
    public interface Callbacks {
        void onCrimeSelected(Crime crime);
    }

    /**
     * Inflate the corresponding list item crime layout in the constructor of the crime holder
     * -> ViewHolder: it does one thing, it hold on to a View, page 168
     */
    private class CrimeHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        private Crime mCrime;
        private TextView mTitleTextView;
        private TextView mDateTextView;
        private ImageView mSolvedImageView;

        public CrimeHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.list_item_crime, parent, false));
            itemView.setOnClickListener(this);

            //extract all the information that I am interested in:
            mTitleTextView = (TextView) itemView.findViewById(R.id.crime_title);
            mDateTextView = (TextView) itemView.findViewById(R.id.crime_date);
            mSolvedImageView = (ImageView) itemView.findViewById(R.id.crime_solved);
        }

        /**
         * Bind function that is called by the adapter to bind the data to the recycleview (list)
         *
         * @param crime
         */
        public void bind(Crime crime) {
            mCrime = crime;
            mTitleTextView.setText(crime.getmTitle());
            mDateTextView.setText(crime.getmDate().toString());
            mSolvedImageView.setVisibility(crime.ismSolved() ? View.VISIBLE : View.GONE);
        }

        @Override
        public void onClick(View v) {
            // start or show new fragment, depends if tablet or phone
            mCallbacks.onCrimeSelected(mCrime);
        }
    }

    private class CrimeAdapter extends RecyclerView.Adapter<CrimeHolder> {
        private List<Crime> mCrimes;

        /**
         * Constructor for the CrimeAdapter, page 174
         *
         * @param crimes
         */
        public CrimeAdapter(List<Crime> crimes) {
            mCrimes = crimes;
        }


        /**
         * ..creates a new ViewHolder
         * It is called by the RecyclerView when it needs a new ViewHolder, page 174
         *
         * @param parent
         * @param viewType
         * @return ViewHolder object (-> CrimeHolder)
         */
        @NonNull
        @Override
        public CrimeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new CrimeHolder(layoutInflater, parent);
        }

        /**
         * to bind it, the adapter fills in the View to reflect the data in the model object.
         * Be always effective in onBindViewHolder, because it is called a lot.
         */
        @Override
        public void onBindViewHolder(@NonNull CrimeHolder crimeHolder, int position) {
            Crime crime = mCrimes.get(position);
            crimeHolder.bind(crime);
        }

        @Override
        public int getItemCount() {
            return mCrimes.size();
        }

        public void setCrimes(List<Crime> crimes) {
            mCrimes = crimes;
        }
    }

}
