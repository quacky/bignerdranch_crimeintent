package com.example.daniel.criminalintent_daniel;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by daniel on 01.10.17.
 */

public class CrimeFragment extends Fragment {

    public static final String ARG_CRIME_ID = "crime_id";
    private static final String DIALOGE_DATE = "DialogDate";
    private static final int REQUEST_CODE_DATE = 0;
    private static final int REQUEST_CONTACT = 1;
    private static final int REQUEST_PHOTO = 2;
    private Crime mCrime;
    private EditText mTitleField;
    private Button mButton;
    private CheckBox mCheckbox;
    private Button mSuspectButton;
    private Button mReportButton;
    private ImageButton mPhotoButton;
    private ImageView mPhotoView;
    private File mPhotoFile;
    private Callbacks mCallbacks;

    /**
     * Creates an arguments bundle, creates a fragment instance (constructor call), attaches
     * the arguments to the constructor
     * The order is important.
     * First Create the bundle, add the arguments.
     * Create Fragment and add arguments to it.
     *
     * @param crimeID
     * @return
     */
    public static CrimeFragment NewInstance(UUID crimeID) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_CRIME_ID, crimeID);
        CrimeFragment fragment = new CrimeFragment();
        fragment.setArguments(args);
        return fragment;
    }
    public interface Callbacks {
        void onCrimeUpdated(Crime crime);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallbacks = (Callbacks) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // retrieve the arguments for this fragment:
        UUID crimeID = (UUID) getArguments().getSerializable(ARG_CRIME_ID);
        mCrime = CrimeLab.get(getActivity()).getCrime(crimeID);
        mPhotoFile = CrimeLab.get(getActivity()).getPhotoFile(mCrime);
    }

    /**
     * Crime instances get modified in CrimeFragment and will need to be written out when
     * CrimeFragment is done.
     * This will update CrimeLab's copy of your crime, page 280
     */
    @Override
    public void onPause() {
        super.onPause();
        updateCrime();
    }

    /**
     * Do the needful if the crime has changed
     */
    private void updateCrime() {
        // write the current changes in the database:
        CrimeLab.get(getActivity()).updateCrime(mCrime);
        // inform hosting activity that something changed
        mCallbacks.onCrimeUpdated(mCrime);
    }

    /**
     * Called if the child fragment/activity is finished and sends a result
     * E.g. for datepickerFragment
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_CODE_DATE) {
            // got a date back
            Date date = (Date) data.getSerializableExtra(DatePickerFragment.EXTRA_DATE);
            mCrime.setmDate(date);
            updateDate();
            updateCrime();
        } else if (requestCode == REQUEST_CONTACT) {
            // got a contact back
            Uri contactUri = data.getData();
            // Specify which fields you want your query to return
            // values for
            String[] queryFields = new String[]{ContactsContract.Contacts.DISPLAY_NAME};

            //perform your query - the contactUri is like a "where" clause here
            Cursor c = getActivity().getContentResolver().query(contactUri, queryFields,
                    null, null, null);

            try {
                // Double check that you actually got results
                if (c.getCount() == 0) {
                    return;
                }

                // PUll out the first column of the first row of data -
                // that is your suspects name
                c.moveToFirst();
                String suspect = c.getString(0);
                mCrime.setmSuspect(suspect);
                mSuspectButton.setText(suspect);
                updateCrime();
            } finally {
                c.close();
            }

        } else if (requestCode == REQUEST_PHOTO) {
            Uri filePathUri = FileProvider.getUriForFile(getContext(),
                    "com.example.daniel.criminalintent_daniel.fileprovider",
                    mPhotoFile);
            getActivity().revokeUriPermission(filePathUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            updatePhotoView();
            updateCrime();
        }
    }

    private void updateDate() {
        mButton.setText(mCrime.getmDate().toString());
    }

    /**
     * New permission model by Android: was needed for emulator with Api 29
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PHOTO) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(), "camera permission granted", Toast.LENGTH_LONG);
                invokeCamera();
            } else {
                Toast.makeText(getContext(), "camera permission not granted", Toast.LENGTH_LONG);
            }
        }
    }

    /**
     * This is the method which inflates the layout for the fragment's view and return the inflated
     * view to the hosting activity. (not onCreate())
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // explicitly inflate the view:
        // attach to root false because I will add it in the code itself.
        View v = inflater.inflate(R.layout.fragment_crime, container, false);

        // change title of crime if user add new text in field:
        mTitleField = v.findViewById(R.id.crime_title);
        mTitleField.setText(mCrime.getmTitle());
        mTitleField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // do nothing
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mCrime.setmTitle(charSequence.toString());
                updateCrime();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                // do nothing
            }
        });


        // camera image view
        mPhotoView = (ImageView) v.findViewById(R.id.crime_photo);
        updatePhotoView();

        // camera button
        mPhotoButton = (ImageButton) v.findViewById(R.id.crime_camera);
        final Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // verify that there is a camera app on the phone:

        final PackageManager packageManager = getActivity().getPackageManager();
        boolean canTakePhoto = mPhotoFile != null &&
                cameraIntent.resolveActivity(packageManager) != null;
        mPhotoButton.setEnabled(canTakePhoto);

        mPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    invokeCamera();
                } else {
                    String[] permissionRequested = {Manifest.permission.CAMERA};
                    requestPermissions(permissionRequested, REQUEST_PHOTO);
                }

            }
        });


        // show date in button
        mButton = v.findViewById(R.id.crime_date);
        updateDate();
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getFragmentManager();
                DatePickerFragment dialog = DatePickerFragment.newInstance(mCrime.getmDate());
                dialog.setTargetFragment(CrimeFragment.this, REQUEST_CODE_DATE);
                dialog.show(manager, DIALOGE_DATE);
            }
        });


        // react to changes in checkbox
        mCheckbox = v.findViewById(R.id.crime_solved);
        mCheckbox.setChecked(mCrime.ismSolved());
        mCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCrime.setmSolved(isChecked);
                updateCrime();
            }
        });


        // Report the crime:
        mReportButton = v.findViewById(R.id.crime_report);
        mReportButton.setText(R.string.send_report);
        mReportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, getCrimeReport());
                i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.crime_report_subject));
                // always use a choose list for the user:
                i = Intent.createChooser(i, getString(R.string.crime_report_subject));
                startActivity(i);
            }
        });

        //Suspect Button:
        mSuspectButton = v.findViewById(R.id.crime_suspect);
        mSuspectButton.setText(R.string.crime_suspect);
        final Intent pickContact = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        mSuspectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(pickContact, REQUEST_CONTACT);
            }
        });

        if (mCrime.getmSuspect() != null) {
            mSuspectButton.setText(mCrime.getmSuspect());
        }

        // verify that there is a contact app on the phone:
        if (packageManager.resolveActivity(pickContact, PackageManager.MATCH_DEFAULT_ONLY) == null) {
            mSuspectButton.setEnabled(false);
        }

        return v;
    }

    private void invokeCamera() {
        final Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getActivity().getPackageManager();

        Uri filePathUri = FileProvider.getUriForFile(getActivity(),
                "com.example.daniel.criminalintent_daniel.fileprovider",
                mPhotoFile);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, filePathUri);


        // grant write access to all camera apps on the device
        List<ResolveInfo> cameraActivities =
                packageManager.queryIntentActivities(cameraIntent,
                        PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo activity : cameraActivities) {
            getActivity().grantUriPermission(activity.activityInfo.packageName,
                    filePathUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        }
        startActivityForResult(cameraIntent, REQUEST_PHOTO);
    }

    /**
     * Method that creates a report based on the crime data
     *
     * @return the report string
     */
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    private String getCrimeReport() {
        String solvedString = null;
        if (mCrime.ismSolved()) {
            solvedString = getString(R.string.crime_report_solved);
        } else {
            solvedString = getString(R.string.crime_report_unsolved);
        }

        // proposal by book: Problem: I was not able to change it to german locale
        // String dateformat = "EEE, MMM dd";
        // String dateString = DateFormat.format(dateformat, mCrime.getmDate()).toString();
        // therfore use the following approach:

        java.text.DateFormat df = DateFormat.getLongDateFormat(getContext());
        String dateString = df.format(mCrime.getmDate());

        String suspect = mCrime.getmSuspect();
        if (suspect == null) {
            suspect = getString(R.string.crime_report_no_suspect);
        } else {
            suspect = getString(R.string.crime_report_suspect);
        }

        String report = getString(R.string.crime_report, mCrime.getmTitle(), dateString, solvedString, suspect);

        return report;
    }

    private void updatePhotoView() {
        if (mPhotoFile == null || !mPhotoFile.exists()) {
            mPhotoView.setImageDrawable(null);
        } else {
            Bitmap bitmap = PictureUtils.getScaledBitmap(mPhotoFile.getPath(), getActivity());
            mPhotoView.setImageBitmap(bitmap);
        }
    }



}
