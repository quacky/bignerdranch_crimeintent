package com.example.daniel.criminalintent_daniel;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;

import com.example.daniel.criminalintent_daniel.CrimeDBSchema.CrimeTable;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Singleton Class which is used as global data storage for all crimes.
 */
public class CrimeLab {
    private static CrimeLab sCrimeLab;
    private Context mContext;
    private SQLiteDatabase mDataBase;

    /**
     * Singleton constructor
     *
     * @param context
     */
    private CrimeLab(Context context) {
        mContext = context.getApplicationContext();
        mDataBase = new CrimeBaseHelper(mContext).getWritableDatabase();
    }

    /**
     * Public constructor for this singleton.
     *
     * @param context
     * @return
     */
    public static CrimeLab get(Context context) {
        if (sCrimeLab == null) {
            sCrimeLab = new CrimeLab(context);
        }
        return sCrimeLab;
    }

    /**
     * Create a ContentValue object that holds the given crime object
     * ContentValues is a key value store class (similar as java hash map)
     * but designed to hold data for SQLite
     *
     * @param crime to be stored in the ContenValue
     * @return ContentValue object that holds the crime object
     */
    private static ContentValues getContenValues(Crime crime) {
        ContentValues values = new ContentValues();
        values.put(CrimeTable.COLS.UUID, crime.getmId().toString());
        values.put(CrimeTable.COLS.date, crime.getmDate().getTime());
        values.put(CrimeTable.COLS.solved, crime.ismSolved() ? 1 : 0);
        values.put(CrimeTable.COLS.Title, crime.getmTitle());
        values.put(CrimeTable.COLS.suspect, crime.getmSuspect());
        return values;
    }

    public List<Crime> getCrimes() {
        List<Crime> crimes = new ArrayList<>();
        // query everything
        CrimeCursorWrapper cursorWrapper = queryCrimes(null, null);
        try {
            cursorWrapper.moveToFirst();
            while (!cursorWrapper.isAfterLast()) {
                // iterate over all rows in the cursor
                crimes.add(cursorWrapper.getCrime());
                cursorWrapper.moveToNext();
            }
        } finally {
            cursorWrapper.close();
        }
        return crimes;
    }

    public Crime getCrime(UUID id) {
        CrimeCursorWrapper cursorWrapper =
                queryCrimes(CrimeTable.COLS.UUID + " = ? ", new String[]{id.toString()});
        Crime crime;
        try {
            if (cursorWrapper.getCount() == 0) {
                return null;
            }
            cursorWrapper.moveToFirst();
            return cursorWrapper.getCrime();
        } finally {
            cursorWrapper.close();
        }
    }

    /**
     * Add a crime to the crime data base.
     *
     * @param crime to add
     */
    public void addCrime(Crime crime) {
        ContentValues values = getContenValues(crime);
        mDataBase.insert(CrimeTable.NAME, null, values);
    }

    /**
     * Replace the crime object with the same uuid in the database with the given crime.
     *
     * @param crime object that should be updated in the database
     */
    public void updateCrime(Crime crime) {
        String uuidString = crime.getmId().toString();
        ContentValues values = getContenValues(crime);

        // the ? in the where clause is a protection agains sql injection attack
        // it will treat the given string as string and not as sql code.
        mDataBase.update(CrimeTable.NAME, values,
                CrimeTable.COLS.UUID + "= ?", new String[]{uuidString});
    }

    /**
     * Reading from the database, page 281
     *
     * @param whereClause
     * @param whereArgs
     * @return a cursor wrapper to the required column at the table. The cursor contains the raw data.
     * The wrapper helps to extract it
     */
    private CrimeCursorWrapper queryCrimes(String whereClause, String[] whereArgs) {
        Cursor cursor = mDataBase.query(CrimeTable.NAME,
                null, //null selects all columns
                whereClause,
                whereArgs,
                null, null, null);
        return new CrimeCursorWrapper(cursor);
    }

    /**
     * @param crime
     * @return provides a complete filepath for the given crime
     */
    public File getPhotoFile(Crime crime) {
        File filesDir = mContext.getFilesDir();
        return new File(filesDir, crime.getPhotFilename());
    }

    /**
     * Use the cursorWrapper class as helper to extract the needed raw data (and write of course)
     * out of the cursor.
     */
    public class CrimeCursorWrapper extends CursorWrapper {

        public CrimeCursorWrapper(Cursor cursor) {
            super(cursor);
        }

        /**
         * Get a crime object out of this cursor
         *
         * @return
         */
        public Crime getCrime() {
            // extract the raw data
            String uuid = getString(getColumnIndex(CrimeTable.COLS.UUID));
            String title = getString(getColumnIndex(CrimeTable.COLS.Title));
            int isSolved = getInt(getColumnIndex(CrimeTable.COLS.solved));
            long date = getLong(getColumnIndex(CrimeTable.COLS.date));
            String suspect = getString(getColumnIndex(CrimeTable.COLS.suspect));

            // create a new crime with the given data
            Crime crime = new Crime(UUID.fromString(uuid));
            crime.setmDate(new Date(date));
            crime.setmSolved(isSolved == 1);
            crime.setmTitle(title);
            crime.setmSuspect(suspect);

            return crime;

        }
    }

}
