package com.example.daniel.criminalintent_daniel;

import java.util.Date;
import java.util.UUID;

/**
 * Created by daniel on 22.09.17.
 */

/**
 * This class represents a single crime with
 * an unique id and a date.
 */
public class Crime {
    private UUID mId;
    private String mTitle;
    private Date mDate;
    private boolean mSolved;


    private String mSuspect;

    public Crime() {
        mId = UUID.randomUUID();
        mDate = new Date();
    }

    /**
     * Create a new crime with a given uuid
     *
     * @param uuid
     */
    public Crime(UUID uuid) {
        mId = uuid;
        mDate = new Date();
    }

    public UUID getmId() {
        return mId;
    }

    public void setmId(UUID mId) {
        this.mId = mId;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public Date getmDate() {
        return mDate;
    }

    public void setmDate(Date mDate) {
        this.mDate = mDate;
    }

    public boolean ismSolved() {
        return mSolved;
    }

    public void setmSolved(boolean mSolved) {
        this.mSolved = mSolved;
    }

    public String getmSuspect() {
        return mSuspect;
    }

    public void setmSuspect(String mSuspect) {
        this.mSuspect = mSuspect;
    }

    /**
     * @return path name for a phot that contains the crime id
     */
    public String getPhotFilename() {
        return "IMG_" + getmId().toString() + ".jpg";
    }
}
