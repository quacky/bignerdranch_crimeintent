package com.example.daniel.criminalintent_daniel;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

public class CrimeListActivity extends SingleFragmentActivity implements CrimeListFragment.Callbacks, CrimeFragment.Callbacks {

    @Override
    protected Fragment createFragment() {
        return new CrimeListFragment();
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_masterdetail;
    }

    @Override
    public void onCrimeSelected(Crime crime) {
        if (findViewById(R.id.detailed_fragment_container) == null) {
            // must be a phone, start the pager
            Intent intent = CrimePagerActivity.newIntent(this, crime.getmId());
            startActivity(intent);
        } else {
            // must be a tablet -> add the crime to the detail fragment
            Fragment detailFragment = CrimeFragment.NewInstance(crime.getmId());
            FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction().replace(R.id.detailed_fragment_container, detailFragment).commit();
        }
    }

    /**
     * Update crime list if something changed in the detail view for tablets
     * @param crime
     */
    @Override
    public void onCrimeUpdated(Crime crime) {
            FragmentManager fm = getSupportFragmentManager();
            CrimeListFragment currentCrimeListFragment = (CrimeListFragment) fm.findFragmentById(R.id.fragment_container);
            currentCrimeListFragment.updateUI();
    }
}
