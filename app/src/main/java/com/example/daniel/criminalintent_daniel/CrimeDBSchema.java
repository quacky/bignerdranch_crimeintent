package com.example.daniel.criminalintent_daniel;

public class CrimeDBSchema {
    public static final class CrimeTable {
        public static final String NAME = "crimes";

        public static final class COLS {
            public static final String UUID = "uuid";
            public static final String Title = "title";
            public static final String date = "date";
            public static final String solved = "solved";
            public static final String suspect = "suspect";
        }

    }
}
